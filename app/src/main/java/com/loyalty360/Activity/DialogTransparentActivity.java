package com.loyalty360.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.loyalty360.Model.BurnResponse;
import com.loyalty360.Utilities.ApplicationConstants;
import com.loyalty360.View.CustomDialog;
import com.loyalty360.View.SMSOfflineDialog;

//Used to show the dialog when the SMS is sent successfully and a response is received from inside a broadcast receiever

public class DialogTransparentActivity extends AppCompatActivity {

    private String[] burnResponse;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        burnResponse = (String[]) getIntent().getSerializableExtra(ApplicationConstants.BURN_RESPONSE);
        if(burnResponse!=null)
        {
            SMSOfflineDialog smsOfflineDialog = new SMSOfflineDialog(this, burnResponse);
            smsOfflineDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            smsOfflineDialog.setCancelable(false);
            smsOfflineDialog.show();

            smsOfflineDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                   /* NationalIDEditText.setText("");
                    PrivateNumberEditText.setText("");*/
                    Intent intent = new Intent(DialogTransparentActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
        }
    }
}

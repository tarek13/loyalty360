package com.loyalty360.Activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;

import com.loyalty360.Adapter.TabViewAdapter;
import com.loyalty360.R;
import com.loyalty360.Utilities.SharedPreferencesUtilities;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabViewAdapter adapter;
    private SharedPreferencesUtilities sharedPreferencesUtilities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferencesUtilities=new SharedPreferencesUtilities(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new TabViewAdapter(getSupportFragmentManager(),this);
        viewPager.setAdapter(adapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(this);
        setupTabIcons();
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putBoolean("FIRSTTIME",false).apply();
    }

    private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.new_transaction_active,
                R.drawable.transactions_history_not_active
        };
        View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_history_transactions, null);

        tabLayout.getTabAt(1).setCustomView(R.layout.custom_tab_new_transactions);
        tabLayout.getTabAt(1).select();

        tabLayout.getTabAt(0).setCustomView(R.layout.custom_tab_history_transactions);


    }
    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        if(tab.getPosition()==0) {
                adapter.updateFragments();
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    protected void onPause() {
        super.onPause();
      //  sharedPreferencesUtilities.saveLoadHistoryFlag(true);
    }
}

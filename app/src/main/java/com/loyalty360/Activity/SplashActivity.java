package com.loyalty360.Activity;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telecom.Call;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loyalty360.Model.ApplicationText;
import com.loyalty360.Model.TransactionHistory;
import com.loyalty360.R;
import com.loyalty360.Utilities.ApplicationConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.loyalty360.Utilities.ApplicationConstants.SIMCARDNUMBER;


public class SplashActivity extends AppCompatActivity  implements ActivityCompat.OnRequestPermissionsResultCallback{

    //Time that the Splash Image is shown
    private static final long SPLASH_TIME_OUT =2000 ;
    Boolean firstTime;
    String versionNumber;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //Check if it is the first time to open the application on this phone
        firstTime=PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("FIRSTTIME",true);
        versionNumber=PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("VERSIONUMBER","1");
        if(firstTime){
            //Set the Default app text to the default values
            try {
                SetDefaultText();
                //After setting default at the firstTime it attempts to get the app text from the server if there is internet
                AttemptToGetTextFromServer();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else
        {
            if(haveNetworkConnection()){
                //Attempts to get the text from server
                AttemptToGetTextFromServer();
            }
            else
            {
                CallHandler();
            }
        }


    }

    /**
     * Function that calls the API that returns the app text
     */
    private void AttemptToGetTextFromServer(){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.setTimeout(30*1000);
        //CHANGE THE URL FOR THE API THAT RETURNS THE APP TEXT
        asyncHttpClient.get("http://stagingcouponzapi.dsquares.com/api/MobileApp/GetMobileConfigruation?version="+versionNumber, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    switch (response.get("ErrorCode").toString()){
                        case "0":
                            //Save the response received to sharedpreferences for further usage
                            SaveJsonReponseToSharedPrefernce(response.toString());
                            CallHandler();
                            break;
                        case "1":
                            CallHandler();
                            break;
                        case "2":
                            Toast.makeText(getApplicationContext(), "Wrong Version number format", Toast.LENGTH_LONG).show();
                            break;
                        case "-1":
                            Toast.makeText(getApplicationContext(), "Unknown Error Occured", Toast.LENGTH_LONG).show();
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                CallHandler();
            }
        });
    }


private void SaveJsonReponseToSharedPrefernce(String response){
    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(ApplicationConstants.APP_TEXT, response).apply();


}

    /**
     * Default APP TEXT
     *
     * @throws JSONException
     */
    private void SetDefaultText() throws JSONException {
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(ApplicationConstants.APP_TEXT, "{\n" +
                "  \"$id\": \"1\",\n" +
                "  \"ErrorCode\": 0,\n" +
                "  \"ErrorDesc\": \"New version available \",\n" +
                "  \"Data\": {\n" +
                "    \"VersionNumber\": \"2\",\n" +
                "    \"BurnTabTitle\": \"  معاملة جديدة\",\n" +
                "    \"BurnTabField1\": \"قم بأدخال آخر 6 ارقام من الرقم القومي\",\n" +
                "    \"BurnTabField2\": \"قم بإدخال الرقم السري\",\n" +
                "    \"BurnTabButton\": \" قم بإدخال الرقم السري\",\n" +
                "    \"BurnTabValidationError\": \"الرقم القومي او الرقم السري فارغ من فضلك ادخل البيانات صحيحه\",\n" +
                "    \"BurnVoucherAPIUrl\": \"https://stagingcouponzapi.dsquares.com/api/Pointing/BurnVoucher\",\n" +
                "    \"PopUpResponseButton\": \"إنهاء\",\n" +
                "    \"SuccessPopUpLine1\": \"تم إستخدام الرقم السري بنجاح\",\n" +
                "    \"SuccessPopUpLine2\": \"بقيمة  {Value} جنيه\",\n" +
                "    \"SuccessPopUpLine3\": \"{TransactionId} رقم القسيمة\",\n" +
                "    \"ErrorCode1PopUp\": \" الرقم السري او الرقم القومي غير صحيح\",\n" +
                "    \"ErrorCode2PopUp\": \"عفوا, الرقم السري منتهي الصلاحية\",\n" +
                "    \"ErrorCode3PopUp\": \"هذا الرقم السري مستخدم من قبل\",\n" +
                "    \"BurnInternetConnectionError\": \"غير متصل بالشبكة، ستصلك رسالة نصية بنتيجة العملية\",\n" +
                "    \"HistoryTabTitle\": \"المعاملات السابقة\",\n" +
                "    \"HistoryTabSubTitle\": \"حتى آخر 50 معاملة\",\n" +
                "    \"HistoryTabLine1\": \"الرقم السري\",\n" +
                "    \"HistoryTabLine2\": \"رقم العملية\",\n" +
                "    \"GetHistoryAPIUrl\": \"https://stagingcouponzapi.dsquares.com/api/Couponz/GetHistory\",\n" +
                "    \"InternetConnectError\": \"عفواً، لا توجد تغطيه\",\n" +
                "    \"ChangeMerchantNum\": \"ادخل رقم التليفون الخاص بك\",\n" +
                "    \"SMSDefaultNumber\": \"44401212121222120\"\n" +
                "  }\n" +
                "}" ).apply();

    }
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    private void CallHandler(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start Main Activity
                //Add here code to retrieve from API

                //Function to retrieve the Sim card Number and save it to shared preferences after checking for permission

                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

    }

}

package com.loyalty360.Fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loyalty360.Adapter.TransactionHistoryListAdapter;
import com.loyalty360.Model.ApplicationText;
import com.loyalty360.Model.TransactionHistory;
import com.loyalty360.R;
import com.loyalty360.Utilities.ApplicationConstants;
import com.loyalty360.Utilities.SharedPreferencesUtilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import cz.msebera.android.httpclient.Header;


public class HistoryTransactionFragment extends Fragment implements Observer {

    private TransactionHistoryListAdapter transactionHistoryListAdapter;
    private ArrayList<TransactionHistory> transactionHistoryArrayList = new ArrayList<>();
    private ProgressDialog progressDialog;
    String simCardPhoneNumber = "";
    SharedPreferencesUtilities sharedPreferencesUtilities;
    TextView history_list_header;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 1234;
    ApplicationText applicationText;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history_transaction, container, false);
        //gets the application text from the shared preference
        try {
            applicationText = new ApplicationText(new JSONObject(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ApplicationConstants.APP_TEXT, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sharedPreferencesUtilities = new SharedPreferencesUtilities(getActivity());
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.transactionHistoryRecyclerView);
        history_list_header = (TextView) rootView.findViewById(R.id.history_list_header);
        history_list_header.setText(applicationText.getHistoryTabSubTitle());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        transactionHistoryListAdapter = new TransactionHistoryListAdapter(getActivity());
        recyclerView.setAdapter(transactionHistoryListAdapter);

        CheckForSMSPermission();
        setProgressDialog();
        return rootView;
    }

    /**
     * Checks if a new transaction has been made , if yes then it reloads the history list from the server , if not it loads from the saved list in the shared
     * prefs
     */
    public void reload() {
        if (sharedPreferencesUtilities.getLoadHistoryFlag()) {
            GetListFromServer();
        } else {
         //   transactionHistoryArrayList.clear();
            transactionHistoryArrayList = sharedPreferencesUtilities.getTransactionHistory();
            transactionHistoryListAdapter.setTransactionHistoryArrayList(transactionHistoryArrayList);
            transactionHistoryListAdapter.notifyDataSetChanged();
        }

    }

    /**
     * Sets the progress dialog
     */
    private void setProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("برجاء الانتظار");
    }

    /**
     * Load the history list from the server
     */
    private void GetListFromServer() {
        //Make sure the latest phone number is loaded
        //Change "201005852627" to whatever default number you would like to be loaded incase of any error
        simCardPhoneNumber = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ApplicationConstants.SP_PHONENUMBER, "201005852627");

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.setTimeout(30 * 1000);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custome_progress_dialog);
        transactionHistoryArrayList.clear();
        asyncHttpClient.get(applicationText.getGetHistoryAPIUrl() + "?simNumber=" + simCardPhoneNumber + ApplicationConstants.HISTORY_URL_2, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++) {
                        transactionHistoryArrayList.add(i, new TransactionHistory(response.getJSONObject(i)));
                    }
                    sharedPreferencesUtilities.removeAllTransactionHistory(transactionHistoryArrayList);
                    sharedPreferencesUtilities.savetransactionHistories(transactionHistoryArrayList);
                    transactionHistoryListAdapter.setTransactionHistoryArrayList(transactionHistoryArrayList);
                    transactionHistoryListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
                sharedPreferencesUtilities.saveLoadHistoryFlag(false);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                sharedPreferencesUtilities.saveLoadHistoryFlag(true);
                progressDialog.dismiss();
                Toast.makeText(getActivity(), applicationText.getInternetConnectError(), Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void update(Observable observable, Object o) {
        if (haveNetworkConnection()) {
            reload();
        } else {
            Toast.makeText(getActivity(), "غير متصل بالشبكة",
                    Toast.LENGTH_LONG).show();
        }
    }
//Checks for active internet connection
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    /**
     * Calls a function to check for the sms and phone permission to handle it at run time incase of Marshamellow +
     */
    public void CheckForSMSPermission() {
        if (Build.VERSION.SDK_INT < 23) {
        } else {
            requestSMSPhonePermission();
        }
    }

    private void requestSMSPhonePermission() {

        int hasSMSPermission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.SEND_SMS);
        int hasPhonePermission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE);

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE}, 1234);
        }
    }

//Grants the permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                // Check if the only required permission has been granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", "SMS & Phone permission has now been granted. Showing result.");
                //    Toast.makeText(getActivity(), "SMS Permission is Granted", Toast.LENGTH_SHORT).show();
                } else {
                    Log.i("Permission", "SMS & Phone permission were NOT granted.");
                }
                break;
        }
    }
}

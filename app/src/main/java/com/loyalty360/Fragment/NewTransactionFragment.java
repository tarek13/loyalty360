package com.loyalty360.Fragment;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loyalty360.Activity.MainActivity;
import com.loyalty360.Activity.SplashActivity;
import com.loyalty360.Model.ApplicationText;
import com.loyalty360.Model.BurnResponse;
import com.loyalty360.R;
import com.loyalty360.Utilities.ApplicationConstants;
import com.loyalty360.Utilities.SharedPreferencesUtilities;
import com.loyalty360.View.CustomDialog;
import com.loyalty360.View.NewPhoneNumberDialog;
import com.loyalty360.View.SMSSuccessDialog;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import cz.msebera.android.httpclient.Header;




public class NewTransactionFragment extends Fragment implements View.OnClickListener {
    String simCardPhoneNumber = "";
    Button useButton;
    EditText NationalIDEditText;
    EditText PrivateNumberEditText;
    TextView nationalIDTextView;
    TextView privateNumberTextView;
    ApplicationText applicationText;
    SMSSuccessDialog smsSuccessDialog;


    BurnResponse burnResponse = new BurnResponse();

    private ProgressDialog progressDialog;
    private SharedPreferencesUtilities sharedPreferencesUtilities;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /**
         * Load the application text after being retrieved at the Splash screen to be used here
         */
        try {
            applicationText = new ApplicationText(new JSONObject(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ApplicationConstants.APP_TEXT, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        View rootView = inflater.inflate(R.layout.fragment_new_transaction, container, false);
        sharedPreferencesUtilities=new SharedPreferencesUtilities(getActivity());
        useButton = (Button) rootView.findViewById(R.id.usedButton);
        NationalIDEditText = (EditText) rootView.findViewById(R.id.nationalIDEditText);
        PrivateNumberEditText = (EditText) rootView.findViewById(R.id.privateNumberEditText);
        nationalIDTextView = (TextView) rootView.findViewById(R.id.nationalIDTextView);
        privateNumberTextView = (TextView) rootView.findViewById(R.id.privateNumberTextView);
        nationalIDTextView.setText(applicationText.getBurnTabField1());
        privateNumberTextView.setText(applicationText.getBurnTabField2());
        useButton.setOnClickListener(this);
        //Get the saved simCardPhoneNumber
        simCardPhoneNumber = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ApplicationConstants.SP_PHONENUMBER, "0000000000");

        setProgressDialog();

        getSMSResponse();

        return rootView;
    }

    /**
     * Hide the progress dialog at the start of the fragment
     */
    private void setProgressDialog(){
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
    }

    /**
     * Creates a new asynchttpclient instance and use it to send the parameters to the NewTransaction API
     *
     * @param nationalId
     * @param couponNumber
     * @param msisdn
     */
    public void SendTransaction(String nationalId, String couponNumber, String msisdn) {
        AsyncHttpClient sendTransactionClient = new AsyncHttpClient();
        //Set timeout
        sendTransactionClient.setTimeout(30*1000);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custome_progress_dialog);


        sendTransactionClient.get(BuildURLString(nationalId, couponNumber, msisdn), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                //Add function to handle response body and parse XML
                try {
                    //Parse the response into our custom class
                    burnResponse = ParseXML(new String(responseBody));
                    //Handle the error code according to the document
                    HandleBurnResponseErrorCode(burnResponse);
                } catch (XmlPullParserException | IOException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                //Get error code and show dialogue accordingly
                progressDialog.dismiss();
                Toast.makeText(getActivity(), applicationText.getInternetConnectError(), Toast.LENGTH_LONG).show();


            }
        });
    }


    /**
     * Creates a URL string for the new Transaction API
     *
     * @param nationalId
     * @param couponNumber
     * @param msisdn
     * @return concatinatedURL
     */
    private String BuildURLString(String nationalId, String couponNumber, String msisdn) {
        String returnedURL = "";
        returnedURL = applicationText.getBurnVoucherAPIUrl() + '?' + "nationalId=" + nationalId + "&couponNumber=" + couponNumber + "&msisdn=" + msisdn;
        return returnedURL;
    }

    /**
     * Checks if the phone is conncted to the internet or not
     * @return true if connected
     */
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    /**
     * Checks if the phone has an active connection in which case it would call the api else it would send using SMS
     */
    public void sendDataToServer() {
        //Make sure the latest phone number is loaded
        simCardPhoneNumber = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ApplicationConstants.SP_PHONENUMBER, "0000000000");
        if (haveNetworkConnection()) {
            SendTransaction(NationalIDEditText.getText().toString(), PrivateNumberEditText.getText().toString(), simCardPhoneNumber);
        } else {
            SendSMSDialog(getActivity(),simCardPhoneNumber,NationalIDEditText.getText().toString(),PrivateNumberEditText.getText().toString());

        }

    }

    @Override
    public void onClick(View view) {
        if (view == useButton) {
            //Checks if the text entered is the hidden password and nationalid to set the phone number
            if(NationalIDEditText.getText().toString().equals(ApplicationConstants.ADMIN_ID)&&(PrivateNumberEditText.getText().toString().equals(ApplicationConstants.ADMIN_PASSWORD)))
            {
                EnterPhoneNumberDialog(getActivity());
            }
            else
            {
                if(isValidData()) {
                    sendDataToServer();
                }else {
                    Toast.makeText(getActivity(), applicationText.getBurnTabValidationError(), Toast.LENGTH_LONG).show();
                }
            }

        }
    }

    /**
     * Validate that the data the user entered is not empty
     * @return true if it is valid
     */
    private boolean isValidData(){
        if (TextUtils.isEmpty(NationalIDEditText.getText())) {
          //  NationalIDEditText.setError("");
            return false;
        }

        //Get content and check for null
        if (TextUtils.isEmpty(PrivateNumberEditText.getText())) {

            return false;
        }
        return  true;
    }

    /**
     * Parse the received response
     * @param xml
     * @return Custom BurnedResponse after being filled with the response
     * @throws XmlPullParserException
     * @throws IOException
     */
    private BurnResponse ParseXML(String xml) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        BurnResponse burnResponse = new BurnResponse();

        xpp.setInput(new StringReader(xml));
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                switch (xpp.getName()) {
                    case "DiscountPercentage":
                        eventType = xpp.next();
                        burnResponse.setDiscountPercentage(xpp.getText());
                        break;
                    case "DiscountValue":
                        eventType = xpp.next();
                        burnResponse.setDiscountValue(xpp.getText());
                        break;
                    case "DiscountValueType":
                        eventType = xpp.next();
                        burnResponse.setDiscountValueType(xpp.getText());
                        break;
                    case "ErrorCode":
                        eventType = xpp.next();
                        burnResponse.setErrorCode(xpp.getText());
                        break;
                    case "Message":
                        eventType = xpp.next();
                        burnResponse.setMessage(xpp.getText());
                        break;
                    case "OfferName":
                        eventType = xpp.next();
                        burnResponse.setOfferName(xpp.getText());
                        break;
                    case "OfferNumber":
                        eventType = xpp.next();
                        burnResponse.setOfferNumber(xpp.getText());
                        break;
                    case "PriceAfter":
                        eventType = xpp.next();
                        burnResponse.setPriceAfter(xpp.getText());
                        break;
                    case "PriceBefore":
                        eventType = xpp.next();
                        burnResponse.setPriceBefore(xpp.getText());
                        break;
                    case "TransactionId":
                        eventType = xpp.next();
                        burnResponse.setTransactionId(xpp.getText());
                        break;
                    case "Value":
                        eventType = xpp.next();
                        burnResponse.setValue(xpp.getText());
                        break;
                }
            }
            eventType = xpp.next();
            System.out.println("End document");

        }
        return burnResponse;
    }

    /**
     * Handles the burnedResponse ErrorCode according to the document by invoking the CustomDialog which handles the different types of errorcodes and responses
     * @param burnResponse
     */
    private void HandleBurnResponseErrorCode(BurnResponse burnResponse)
    {
        if(burnResponse!=null)
        {
            CustomDialog customDialog=new CustomDialog(getActivity(),burnResponse);
            customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            customDialog.setCancelable(false);
            customDialog.show();
            customDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    //Reset the text in the text fields
                    NationalIDEditText.setText("");
                    PrivateNumberEditText.setText("");
                }
            });
        }
    }

    /**
     * Invokes the EnterPhoneNumber dialog to enter a new phone number
     * @param activity
     */
    private void EnterPhoneNumberDialog(Activity activity){
        NewPhoneNumberDialog newPhoneNumberDialog=new NewPhoneNumberDialog(activity);
        newPhoneNumberDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        newPhoneNumberDialog.show();
        newPhoneNumberDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                NationalIDEditText.setText("");
                PrivateNumberEditText.setText("");
            }
        });
    }

    /**
     * calls the send SMS function with the parameters from the Edittext
     * @param activity
     * @param simCardPhoneNumber
     * @param NationalID
     * @param PrivateNumber
     */
    private void SendSMSDialog(Activity activity,String simCardPhoneNumber,String NationalID,String PrivateNumber ){
        sendSms(simCardPhoneNumber + ',' + NationalID + ',' + PrivateNumber, applicationText.getSMSDefaultNumber());
        NationalIDEditText.setText("");
        PrivateNumberEditText.setText("");

    }

    /**
     * Sends the SMS  using the android SmsManager
     * @param strSMSBody
     * @param strReceipentsList
     */
    private void sendSms(String strSMSBody,String strReceipentsList){
        SmsManager sms = SmsManager.getDefault();
        List<String> messages = sms.divideMessage(strSMSBody);
        for (String message : messages) {
            sms.sendTextMessage(strReceipentsList, null, message, PendingIntent.getBroadcast(
                    getActivity(), 0, new Intent(ApplicationConstants.ACTION_SMS_SENT), 0), null);
        }
    }

    /**
     * Called when the sms is sent successfully
     * @param activity
     */
    private void SMSSuccessDialog(Activity activity) {
        smsSuccessDialog = new SMSSuccessDialog(activity);
        smsSuccessDialog.setCancelable(false);
        smsSuccessDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        smsSuccessDialog.show();
        smsSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                NationalIDEditText.setText("");
                PrivateNumberEditText.setText("");
            }
        });
    }

    /**
     * Handles the sms response
     */
    private void getSMSResponse(){
        getActivity().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = null;
                boolean error = true;
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        message = "Message sent!";
                        //Invokes the SMS Success Dialog
                        SMSSuccessDialog(getActivity());
                        CallHandler();
                        error = false;
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        message = "Error.";
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        message = "Error: No service.";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        message = "Error: Null PDU.";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        message = "Error: Radio off.";
                        break;
                }


            }
        }, new IntentFilter(ApplicationConstants.ACTION_SMS_SENT));
    }

    private void CallHandler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Delayed close for the Dialog

                if (smsSuccessDialog.isShowing()) {
                    smsSuccessDialog.dismiss();
                }

            }
        }, 3000);

    }

}





package com.loyalty360.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.loyalty360.Model.TransactionHistory;
import com.loyalty360.R;
import com.loyalty360.View.TransactionHistoryViewHolder;

import java.text.ParseException;
import java.util.ArrayList;


public class TransactionHistoryListAdapter extends RecyclerView.Adapter<TransactionHistoryViewHolder> {

    private  Context context;
    private ArrayList<TransactionHistory> transactionHistoryArrayList;

    public TransactionHistoryListAdapter(Context context) {
        super();
        this.context=context;
    }

    public ArrayList<TransactionHistory> getTransactionHistoryArrayList() {
        return transactionHistoryArrayList;
    }

    public void setTransactionHistoryArrayList(ArrayList<TransactionHistory> transactionHistoryArrayList) {
        this.transactionHistoryArrayList = transactionHistoryArrayList;
        notifyDataSetChanged();
    }

    @Override
    public TransactionHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_transaction_history, parent, false);
        return new TransactionHistoryViewHolder(v,context);
    }

    @Override
    public void onBindViewHolder(TransactionHistoryViewHolder holder, int position) {
        try {
            holder.setTransactionHistory(getItem(position),String.valueOf(position+1));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public TransactionHistory getItem(int i) {
        return transactionHistoryArrayList.get(i);
    }

    @Override
    public int getItemCount() {
        if (transactionHistoryArrayList == null)
            return 0;
        return transactionHistoryArrayList.size();
    }


}

package com.loyalty360.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.loyalty360.Fragment.HistoryTransactionFragment;
import com.loyalty360.Fragment.NewTransactionFragment;
import com.loyalty360.Utilities.FragmentObserver;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

//Switches between the newtransaction tab and the gethistory tab

public class TabViewAdapter extends FragmentPagerAdapter {

    private static final int NUMBER_OF_TABS = 2;

    private static final int NEW_TRANSACTION_TAB = 1;
    private static final int TRANSACTION_HISTORY_TAB = 0;
    private Context context;
    private Observable mObservers = new FragmentObserver();

    public TabViewAdapter(FragmentManager fm,Context context) {
        super(fm);
        this.context=context;
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        mObservers.deleteObservers(); // Clear existing observers.

        switch (position) {
            case NEW_TRANSACTION_TAB:
                fragment= new NewTransactionFragment();
                break;
            case TRANSACTION_HISTORY_TAB:
                fragment = new HistoryTransactionFragment();
                if(fragment instanceof Observer)
                    mObservers.addObserver((Observer) fragment);
                break;
            default:
                break;
        }
        return fragment;
    }



    public void updateFragments() {
        mObservers.notifyObservers();
    }
    @Override
    public int getCount() {
        return NUMBER_OF_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        // return null to display only the icon
        return null;

    }
}

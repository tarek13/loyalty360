package com.loyalty360.Model;

import com.google.gson.JsonObject;
import com.loyalty360.Utilities.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class to hold the application text and contains a constructor that parses the json into the class directly
 */
public class ApplicationText {
    private String $id;
    private String ErrorCode;
    private String ErrorDesc;
    private String VersionNumber;
    private String BurnTabTitle;
    private String BurnTabField1;
    private String BurnTabField2;
    private String BurnTabButton;
    private String BurnTabValidationError;
    private String BurnVoucherAPIUrl;
    private String SuccessPopUpLine1;
    private String SuccessPopUpLine2;
    private String PopUpResponseButton;
    private String SuccessPopUpLine3;
    private String ErrorCode1PopUp;
    private String ErrorCode2PopUp;
    private String ErrorCode3PopUp;
    private String BurnInternetConnectionError;
    private String HistoryTabTitle;
    private String HistoryTabSubTitle;
    private String HistoryTabLine1;
    private String HistoryTabLine2;
    private String GetHistoryAPIUrl;
    private String InternetConnectError;
    private String ChangeMerchantNum;
    private String SMSDefaultNumber;

    public ApplicationText(JSONObject jsonObject) throws JSONException {
        JSONObject dataJsonObject = jsonObject.getJSONObject("Data");
        this.$id = jsonObject.getString("$id");
        this.ErrorCode = jsonObject.getString("ErrorCode");
        this.ErrorDesc = jsonObject.getString("ErrorDesc");

        this.VersionNumber = dataJsonObject.getString("VersionNumber");
        this.BurnTabTitle = dataJsonObject.getString("BurnTabTitle");
        this.BurnTabField1 = dataJsonObject.getString("BurnTabField1");
        this.BurnTabField2 = dataJsonObject.getString("BurnTabField2");
        this.BurnTabButton = dataJsonObject.getString("BurnTabButton");
        this.BurnTabValidationError = dataJsonObject.getString("BurnTabValidationError");
        this.BurnVoucherAPIUrl = dataJsonObject.getString("BurnVoucherAPIUrl");
        this.SuccessPopUpLine1 = dataJsonObject.getString("SuccessPopUpLine1");
        this.SuccessPopUpLine2 = dataJsonObject.getString("SuccessPopUpLine2");
        this.PopUpResponseButton = dataJsonObject.getString("PopUpResponseButton");
        this.SuccessPopUpLine3 = dataJsonObject.getString("SuccessPopUpLine3");
        this.ErrorCode1PopUp = dataJsonObject.getString("ErrorCode1PopUp");
        this.ErrorCode2PopUp = dataJsonObject.getString("ErrorCode2PopUp");
        this.ErrorCode3PopUp = dataJsonObject.getString("ErrorCode3PopUp");
        this.BurnInternetConnectionError = dataJsonObject.getString("BurnInternetConnectionError");
        this.HistoryTabTitle = dataJsonObject.getString("HistoryTabTitle");
        this.HistoryTabSubTitle = dataJsonObject.getString("HistoryTabSubTitle");
        this.HistoryTabLine1 = dataJsonObject.getString("HistoryTabLine1");
        this.HistoryTabLine2 = dataJsonObject.getString("HistoryTabLine2");
        this.GetHistoryAPIUrl = dataJsonObject.getString("GetHistoryAPIUrl");
        this.InternetConnectError = dataJsonObject.getString("InternetConnectError");
        this.ChangeMerchantNum = dataJsonObject.getString("ChangeMerchantNum");
        this.SMSDefaultNumber = dataJsonObject.getString("SMSDefaultNumber");

    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }

    public String getErrorDesc() {
        return ErrorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        ErrorDesc = errorDesc;
    }

    public String getVersionNumber() {
        return VersionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        VersionNumber = versionNumber;
    }

    public String getBurnTabTitle() {
        return BurnTabTitle;
    }

    public void setBurnTabTitle(String burnTabTitle) {
        BurnTabTitle = burnTabTitle;
    }

    public String getBurnTabField1() {
        return BurnTabField1;
    }

    public void setBurnTabField1(String burnTabField1) {
        BurnTabField1 = burnTabField1;
    }

    public String getBurnTabField2() {
        return BurnTabField2;
    }

    public void setBurnTabField2(String burnTabField2) {
        BurnTabField2 = burnTabField2;
    }

    public String getBurnTabButton() {
        return BurnTabButton;
    }

    public void setBurnTabButton(String burnTabButton) {
        BurnTabButton = burnTabButton;
    }

    public String getBurnTabValidationError() {
        return BurnTabValidationError;
    }

    public void setBurnTabValidationError(String burnTabValidationError) {
        BurnTabValidationError = burnTabValidationError;
    }

    public String getBurnVoucherAPIUrl() {
        return BurnVoucherAPIUrl;
    }

    public void setBurnVoucherAPIUrl(String burnVoucherAPIUrl) {
        BurnVoucherAPIUrl = burnVoucherAPIUrl;
    }

    public String getSuccessPopUpLine1() {
        return SuccessPopUpLine1;
    }

    public void setSuccessPopUpLine1(String successPopUpLine1) {
        SuccessPopUpLine1 = successPopUpLine1;
    }

    public String getSuccessPopUpLine2() {
        return SuccessPopUpLine2;
    }

    public void setSuccessPopUpLine2(String successPopUpLine2) {
        SuccessPopUpLine2 = successPopUpLine2;
    }

    public String getPopUpResponseButton() {
        return PopUpResponseButton;
    }

    public void setPopUpResponseButton(String popUpResponseButton) {
        PopUpResponseButton = popUpResponseButton;
    }

    public String getSuccessPopUpLine3() {
        return SuccessPopUpLine3;
    }

    public void setSuccessPopUpLine3(String successPopUpLine3) {
        SuccessPopUpLine3 = successPopUpLine3;
    }

    public String getErrorCode1PopUp() {
        return ErrorCode1PopUp;
    }

    public void setErrorCode1PopUp(String errorCode1PopUp) {
        ErrorCode1PopUp = errorCode1PopUp;
    }

    public String getErrorCode2PopUp() {
        return ErrorCode2PopUp;
    }

    public void setErrorCode2PopUp(String errorCode2PopUp) {
        ErrorCode2PopUp = errorCode2PopUp;
    }

    public String getErrorCode3PopUp() {
        return ErrorCode3PopUp;
    }

    public void setErrorCode3PopUp(String errorCode3PopUp) {
        ErrorCode3PopUp = errorCode3PopUp;
    }

    public String getBurnInternetConnectionError() {
        return BurnInternetConnectionError;
    }

    public void setBurnInternetConnectionError(String burnInternetConnectionError) {
        BurnInternetConnectionError = burnInternetConnectionError;
    }

    public String getHistoryTabTitle() {
        return HistoryTabTitle;
    }

    public void setHistoryTabTitle(String historyTabTitle) {
        HistoryTabTitle = historyTabTitle;
    }

    public String getHistoryTabSubTitle() {
        return HistoryTabSubTitle;
    }

    public void setHistoryTabSubTitle(String historyTabSubTitle) {
        HistoryTabSubTitle = historyTabSubTitle;
    }

    public String getHistoryTabLine1() {
        return HistoryTabLine1;
    }

    public void setHistoryTabLine1(String historyTabLine1) {
        HistoryTabLine1 = historyTabLine1;
    }

    public String getHistoryTabLine2() {
        return HistoryTabLine2;
    }

    public void setHistoryTabLine2(String historyTabLine2) {
        HistoryTabLine2 = historyTabLine2;
    }

    public String getGetHistoryAPIUrl() {
        return GetHistoryAPIUrl;
    }

    public void setGetHistoryAPIUrl(String getHistoryAPIUrl) {
        GetHistoryAPIUrl = getHistoryAPIUrl;
    }

    public String getInternetConnectError() {
        return InternetConnectError;
    }

    public void setInternetConnectError(String internetConnectError) {
        InternetConnectError = internetConnectError;
    }

    public String getChangeMerchantNum() {
        return ChangeMerchantNum;
    }

    public void setChangeMerchantNum(String changeMerchantNum) {
        ChangeMerchantNum = changeMerchantNum;
    }

    public String getSMSDefaultNumber() {
        return SMSDefaultNumber;
    }

    public void setSMSDefaultNumber(String SMSDefaultNumber) {
        this.SMSDefaultNumber = SMSDefaultNumber;
    }
}

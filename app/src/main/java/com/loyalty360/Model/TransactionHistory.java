package com.loyalty360.Model;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Class to hold the history parameters and contains a constructor that parses the json into the class directly
 */

public class TransactionHistory {
    private String $id;
    private String CouponNumber;
    private String BurnDateTime;
    private String CreatedDateTime;
    private String TransactionId;
    private String CustomerMsisdn;

    public TransactionHistory(JSONObject jsonObject) throws JSONException {
        this.$id=jsonObject.getString("$id");
        this.CouponNumber=jsonObject.getString("CouponNumber");
        this.BurnDateTime=jsonObject.getString("BurnDateTime");
        this.CreatedDateTime=jsonObject.getString("CreatedDateTime");
        this.TransactionId=jsonObject.getString("TransactionId");
        this.CustomerMsisdn=jsonObject.getString("CustomerMsisdn");
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getCouponNumber() {
        return CouponNumber;
    }

    public void setCouponNumber(String couponNumber) {
        CouponNumber = couponNumber;
    }

    public String getBurnDateTime() {
        return BurnDateTime;
    }

    public void setBurnDateTime(String burnDateTime) {
        BurnDateTime = burnDateTime;
    }

    public String getCreatedDateTime() {
        return CreatedDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        CreatedDateTime = createdDateTime;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getCustomerMsisdn() {
        return CustomerMsisdn;
    }

    public void setCustomerMsisdn(String customerMsisdn) {
        CustomerMsisdn = customerMsisdn;
    }
}

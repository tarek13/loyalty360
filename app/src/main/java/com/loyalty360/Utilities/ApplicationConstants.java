package com.loyalty360.Utilities;


public class ApplicationConstants {


    //CHANGE TO DESIRED ID AND PASSWORD FOR PHONE NUMBER CHANGE
    public static final String ADMIN_ID="123";
    public static final String ADMIN_PASSWORD="123";


    //Part 2 of the history api url
    public static final String HISTORY_URL_2="&pageNumber=0&pageSize=40";
    public static final String DATE_FORMAT="yyyy-MM-dd HH:mm:ss.SSS";


    public static final String ACTION_SMS_SENT = "com.techblogon.android.apis.os.SMS_SENT_ACTION";
    public static final String BURN_URL = "https://stagingcouponzapi.dsquares.com/api/Pointing/BurnVoucher?";
    public static final String SP_PHONENUMBER = "PHONENUMBER";
    public static final String SIMCARDNUMBER = "SIMCARDNUMBER";
    public static final String APP_TEXT = "APPTEXT";
    public static String LOAD_HISTORY="load_history";
    public static String TRANSACTION_HISTORY="transaction_history";
    public static String BURN_RESPONSE="BURN_RESPONSE";

}

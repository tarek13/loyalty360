package com.loyalty360.Utilities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.loyalty360.Activity.DialogTransparentActivity;
import com.loyalty360.Activity.MainActivity;
import com.loyalty360.Model.ApplicationText;
import com.loyalty360.Model.BurnResponse;
import com.loyalty360.View.CustomDialog;

import org.json.JSONException;
import org.json.JSONObject;


//BroadCast Receiver that waits for an SMS to be sent when a successful sms is sent to the api
public class IncomingSms extends BroadcastReceiver {

    final SmsManager sms = SmsManager.getDefault();
    ApplicationText applicationText;
    Context context;


    public void onReceive(Context context, Intent intent) {
        //Get apptext
        try {
            applicationText = new ApplicationText(new JSONObject(PreferenceManager.getDefaultSharedPreferences(context).getString(ApplicationConstants.APP_TEXT, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.context = context;


        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);


                    // Show Alert
                    // int duration = Toast.LENGTH_LONG;
                    if (senderNum.contains(applicationText.getSMSDefaultNumber())) {
                        String[] responseStringSeparated;
                        responseStringSeparated = message.split(",");
                        HandleBurnResponseErrorCode(responseStringSeparated, context);

                    }
            /*        Toast toast = Toast.makeText(context,
                            "senderNum: "+ senderNum + ", message: " + message, duration);
                    toast.show();*/

                }
            }

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);

        }
    }

    /**
     * Handles the error codes and invokes the dialog which shows the results
     *
     * @param burnResponse
     * @param context
     */
    private void HandleBurnResponseErrorCode(String[] burnResponse, Context context) {
        if (burnResponse != null) {
            Intent intent = new Intent(context, DialogTransparentActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(ApplicationConstants.BURN_RESPONSE, burnResponse);
            context.getApplicationContext().startActivity(intent);

        }

    }
}
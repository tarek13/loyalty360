package com.loyalty360.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.loyalty360.Model.TransactionHistory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Used to allow a new load of the history fragment when a successful attempt is made using the newTransaction button
public class SharedPreferencesUtilities {
    private Context context;
    public SharedPreferencesUtilities(Context context){
        this.context=context;
    }
    public  void saveLoadHistoryFlag(boolean loadHistoryTransaction) {

        SharedPreferences settings = context.getSharedPreferences(ApplicationConstants.LOAD_HISTORY, Context.MODE_PRIVATE); //1
        SharedPreferences.Editor editor  = settings.edit(); //2

        editor.putBoolean(ApplicationConstants.LOAD_HISTORY, loadHistoryTransaction); //3
        editor.apply(); //4
    }

    public boolean getLoadHistoryFlag() {
        SharedPreferences   settings = context.getSharedPreferences(ApplicationConstants.LOAD_HISTORY, Context.MODE_PRIVATE); //1
        boolean loadHistoryTransaction = settings.getBoolean(ApplicationConstants.LOAD_HISTORY, true); //2
        return loadHistoryTransaction;
    }
    // This four methods are used for maintaining favorites.
    public void savetransactionHistories(List<TransactionHistory> transactionHistoryList) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(ApplicationConstants.TRANSACTION_HISTORY,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
      String jsonTransactionHistoryList = gson.toJson(transactionHistoryList);

        editor.putString(ApplicationConstants.TRANSACTION_HISTORY, jsonTransactionHistoryList);

        editor.apply();
    }

    public void addTransactionHistory(TransactionHistory transactionHistory) {
        List<TransactionHistory> transactionHistories = getTransactionHistory();
        if (transactionHistories == null)
            transactionHistories = new ArrayList<TransactionHistory>();
        transactionHistories.add(transactionHistory);
        savetransactionHistories(transactionHistories);
    }

    public void removeTransactionHistory(TransactionHistory transactionHistory) {
        ArrayList<TransactionHistory> transactionHistories = getTransactionHistory();
        if (transactionHistories != null) {
            transactionHistories.remove(transactionHistory);
            savetransactionHistories(transactionHistories);
        }
    }
    public void removeAllTransactionHistory(ArrayList<TransactionHistory> transactionHistory) {
        ArrayList<TransactionHistory> transactionHistories = getTransactionHistory();
        if (transactionHistories != null) {
            transactionHistories.removeAll(transactionHistory);
            savetransactionHistories(transactionHistories);
        }
    }

    public ArrayList<TransactionHistory> getTransactionHistory() {
        SharedPreferences settings;
        List<TransactionHistory> transactionHistories;

        settings = context.getSharedPreferences(ApplicationConstants.TRANSACTION_HISTORY,
                Context.MODE_PRIVATE);

        if (settings.contains(ApplicationConstants.TRANSACTION_HISTORY)) {
            String jsonFavorites = settings.getString(ApplicationConstants.TRANSACTION_HISTORY, null);
            Gson gson = new Gson();
            TransactionHistory[] transactionHistoryItem = gson.fromJson(jsonFavorites,
                    TransactionHistory[].class);

            transactionHistories = Arrays.asList(transactionHistoryItem);
            transactionHistories = new ArrayList<TransactionHistory>(transactionHistories);
        } else
            return null;

        return (ArrayList<TransactionHistory>) transactionHistories;
    }
}

package com.loyalty360.View;

import android.content.Context;
import java.text.SimpleDateFormat;

import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.loyalty360.Model.ApplicationText;
import com.loyalty360.Model.TransactionHistory;
import com.loyalty360.R;
import com.loyalty360.Utilities.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class TransactionHistoryViewHolder extends RecyclerView.ViewHolder {

    private Context context;
    private TransactionHistory transactionHistory;
    private TextView transactionDateTextView;
    private TextView transactionIDTextView;
    private TextView transactionPrivateNumberTextView;
    private TextView historyIndexNumber;
    private TextView list_private_number;
    private TextView list_transaction_number;
    private String position;
    ApplicationText applicationText;

    public TransactionHistoryViewHolder(View itemView,Context context) {
        super(itemView);
        this.context=context;
        transactionDateTextView=(TextView)itemView.findViewById(R.id.transactionDateTextView);
        transactionIDTextView=(TextView)itemView.findViewById(R.id.transactionIDTextView);
        transactionPrivateNumberTextView=(TextView)itemView.findViewById(R.id.transactionPrivateNumberTextView);
        list_private_number = (TextView) itemView.findViewById(R.id.list_private_number);
        list_transaction_number = (TextView) itemView.findViewById(R.id.list_transaction_number);
        historyIndexNumber=(TextView)itemView.findViewById(R.id.history_index_number);

    }

    public void setTransactionHistory(TransactionHistory transactionHistory,String position) throws ParseException {
        this.transactionHistory = transactionHistory;
        this.position=position;
        fillData();
    }

    private void fillData() throws ParseException {
        try {
            applicationText = new ApplicationText(new JSONObject(PreferenceManager.getDefaultSharedPreferences(this.context).getString(ApplicationConstants.APP_TEXT, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
       String date= FormatDate(transactionHistory.getBurnDateTime());
        this.transactionDateTextView.setText(date);
        this.transactionIDTextView.setText(String.valueOf(transactionHistory.getTransactionId()));
        this.transactionPrivateNumberTextView.setText(String.valueOf(transactionHistory.getCouponNumber()));
        this.historyIndexNumber.setText(this.position);
        this.list_private_number.setText(applicationText.getHistoryTabLine1() + ": ");
        this.list_transaction_number.setText(applicationText.getHistoryTabLine2() + ":");
    }

    /**
     * Date Formatter
     * @param burnedDate
     * @return formated date
     * @throws ParseException
     */
    private String FormatDate(String burnedDate) throws ParseException {
        burnedDate=burnedDate.replace("T"," ");
        SimpleDateFormat format = new SimpleDateFormat(ApplicationConstants.DATE_FORMAT);
        Date newDate = format.parse(burnedDate);

        format = new SimpleDateFormat("dd MMMM  hh:mm", Locale.US);
        String date = format.format(newDate);
        return date;
    }

}

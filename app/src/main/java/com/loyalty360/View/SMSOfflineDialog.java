package com.loyalty360.View;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.loyalty360.Model.ApplicationText;
import com.loyalty360.R;
import com.loyalty360.Utilities.ApplicationConstants;
import com.loyalty360.Utilities.SharedPreferencesUtilities;

import org.json.JSONException;
import org.json.JSONObject;



public class SMSOfflineDialog extends Dialog implements View.OnClickListener {

    public Context activity;
    public Dialog dialog;
    public Button finishButton;
    private String[] burnResponse;
    private TextView cd_error_message;
    private TextView cd_transactionNumber;
    private TextView cd_transationAmount;
    private ImageView cd_image;
    private SharedPreferencesUtilities sharedPreferencesUtilities;
    ApplicationText applicationText;


    public SMSOfflineDialog(Context activity, String[] burnResponse) {

        super(activity);
        this.activity = activity;
        this.burnResponse = burnResponse;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            applicationText = new ApplicationText(new JSONObject(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ApplicationConstants.APP_TEXT, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        sharedPreferencesUtilities = new SharedPreferencesUtilities(activity);
        finishButton = (Button) findViewById(R.id.finishDialogButton);
        cd_transactionNumber = (TextView) findViewById(R.id.cd_transactionNumber);
        cd_transationAmount = (TextView) findViewById(R.id.cd_transationAmount);
        cd_error_message = (TextView) findViewById(R.id.cd_error_message);
        cd_image = (ImageView) findViewById(R.id.cd_image);

        cd_error_message.setText(burnResponse[1]);


        try {
            cd_transactionNumber.setText(burnResponse[2]);
            cd_transationAmount.setText(burnResponse[3]);
            sharedPreferencesUtilities.saveLoadHistoryFlag(true);
        } catch (ArrayIndexOutOfBoundsException e) {

        }



        switch (burnResponse[0]) {
            case "0":
                cd_image.setImageResource(R.drawable.success_icon);
                break;
            case "1":
                cd_image.setImageResource(R.drawable.failed_icon);
                break;
            case "2":
                cd_image.setImageResource(R.drawable.expired_icon);
                break;
            case "3":
                cd_image.setImageResource(R.drawable.used_icon);
                break;
            case "4":
                cd_image.setImageResource(R.drawable.failed_icon);
                break;
        }


        finishButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.finishDialogButton:
                dismiss();
        }


    }
}

package com.loyalty360.View;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loyalty360.Model.ApplicationText;
import com.loyalty360.R;
import com.loyalty360.Utilities.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

//Not used anymore but kept for reference
@Deprecated
public class SMSConfirmationDialog extends Dialog implements View.OnClickListener {

    public Activity activity;
    public Dialog dialog;
    public Button send_sms_yes;
    public Button send_sms_no;
    TextView enter_phone_number;
    String simCardPhoneNumber;
    String NationalID;
    String PrivateNumber;
    ApplicationText applicationText;



    public SMSConfirmationDialog(Activity activity,String simCardPhoneNumber,String NationalID,String PrivateNumber) {


        super(activity);
        this.activity=activity;
        this.simCardPhoneNumber=simCardPhoneNumber;
        this.NationalID=NationalID;
        this.PrivateNumber=PrivateNumber;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            applicationText = new ApplicationText(new JSONObject(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ApplicationConstants.APP_TEXT, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sms_dialog);
        send_sms_yes = (Button) findViewById(R.id.send_sms_yes);
        send_sms_no = (Button) findViewById(R.id.send_sms_no);
        enter_phone_number = (TextView) findViewById(R.id.enter_phone_number);
        enter_phone_number.setText(applicationText.getBurnInternetConnectionError());
        send_sms_yes.setOnClickListener(this);
        send_sms_no.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //Save number to shared preference
            case R.id.send_sms_yes:
                sendSms(simCardPhoneNumber + ',' + NationalID + ',' + PrivateNumber, applicationText.getSMSDefaultNumber());
                dismiss();
                break;
            case R.id.send_sms_no:
                dismiss();
        }


    }
    private void sendSms(String strSMSBody,String strReceipentsList){
        SmsManager sms = SmsManager.getDefault();
        List<String> messages = sms.divideMessage(strSMSBody);
        for (String message : messages) {
            sms.sendTextMessage(strReceipentsList, null, message, PendingIntent.getBroadcast(
                    this.activity, 0, new Intent(ApplicationConstants.ACTION_SMS_SENT), 0), null);
        }
    }
}

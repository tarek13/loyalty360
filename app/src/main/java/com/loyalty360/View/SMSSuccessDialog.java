package com.loyalty360.View;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.loyalty360.Model.ApplicationText;
import com.loyalty360.Model.BurnResponse;
import com.loyalty360.R;
import com.loyalty360.Utilities.ApplicationConstants;
import com.loyalty360.Utilities.SharedPreferencesUtilities;

import org.json.JSONException;
import org.json.JSONObject;


public class SMSSuccessDialog extends Dialog implements View.OnClickListener {

    public Activity activity;
    public Dialog dialog;
    public Button finishButton;
    public TextView sms_success_dialog_text;

    ApplicationText applicationText;


    public SMSSuccessDialog(Activity activity) {

        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            applicationText = new ApplicationText(new JSONObject(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ApplicationConstants.APP_TEXT, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sms_dialog_success);

        finishButton = (Button) findViewById(R.id.finishDialogButton_sms_success);
        sms_success_dialog_text = (TextView) findViewById(R.id.sms_success_dialog_text);
        sms_success_dialog_text.setText(applicationText.getBurnInternetConnectionError());//TODO:Change if needed later
        // sms_success_dialog_text.setText(applicationText.getSMSTEXTSUCCESS());//TODO:Change if needed later

        finishButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.finishDialogButton_sms_success:
                dismiss();
        }


    }
}

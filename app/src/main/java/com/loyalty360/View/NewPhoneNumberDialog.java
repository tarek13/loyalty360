package com.loyalty360.View;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loyalty360.Model.ApplicationText;
import com.loyalty360.Model.BurnResponse;
import com.loyalty360.R;
import com.loyalty360.Utilities.ApplicationConstants;
import com.loyalty360.Utilities.SharedPreferencesUtilities;

import org.json.JSONException;
import org.json.JSONObject;

//Dialog to set the phone number using the secret national id and password then save it to shared prefernce
public class NewPhoneNumberDialog extends Dialog implements View.OnClickListener {

    public Activity activity;
    public Dialog dialog;
    public Button phone_number_finish;
    public EditText enter_phone_number;
    ApplicationText applicationText;
    private SharedPreferencesUtilities sharedPreferencesUtilities;


    public NewPhoneNumberDialog(Activity activity) {

        super(activity);
        this.activity=activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            applicationText = new ApplicationText(new JSONObject(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(ApplicationConstants.APP_TEXT, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.enter_phone_dialog);
        sharedPreferencesUtilities=new SharedPreferencesUtilities(activity);

        phone_number_finish = (Button) findViewById(R.id.phone_number_finish);
        enter_phone_number = (EditText) findViewById(R.id.enter_phone_number);
        enter_phone_number.setHint(applicationText.getChangeMerchantNum());
        phone_number_finish.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //Save number to shared preference
            case R.id.phone_number_finish:
                if(enter_phone_number.getText().length()==12&&enter_phone_number.getText().toString().startsWith("201")){
                    PreferenceManager.getDefaultSharedPreferences(this.activity).edit().putString(ApplicationConstants.SP_PHONENUMBER,enter_phone_number.getText().toString()).apply();
                    sharedPreferencesUtilities.saveLoadHistoryFlag(true);
                    dismiss();
                }
                else
                {
                    Toast.makeText(activity,"ادخل رقم تليفون صحيح",Toast.LENGTH_LONG).show();

                }
        }


    }
}


